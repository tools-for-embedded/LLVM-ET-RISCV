#!/usr/bin/env bash
# NOTE: CMAKE_SYSTEM_NAME is set to linux to allow the configure step to
#       correctly validate that clang works for cross compiling
for CRT_MULTILIB in $(${BUILDPREFIX}/llvm/bin/clang -target riscv32-unknown-elf -print-multi-lib 2>/dev/null); do
  CRT_MULTILIB_DIR=$(echo ${CRT_MULTILIB} | sed 's/;.*//')
  CRT_MULTILIB_OPT=$(echo ${CRT_MULTILIB} | sed 's/.*;//' | sed 's/@/-/' | sed 's/@/ -/g')
  CRT_MULTILIB_BDIR=$(echo ${CRT_MULTILIB} | sed 's/.*;//' | sed 's/@/_/g')
  echo "Multilib: \"${CRT_MULTILIB_DIR}\" -> \"${CRT_MULTILIB_OPT}\""

  mkdir -p ${BUILDPREFIX}/compiler-rt32${CRT_MULTILIB_BDIR}
  cd ${BUILDPREFIX}/compiler-rt32${CRT_MULTILIB_BDIR}
  cmake -G"Unix Makefiles"                                                     \
      -DCMAKE_SYSTEM_NAME=Linux                                                \
      -DCMAKE_INSTALL_PREFIX=${BUILDPREFIX}/compiler-rt32${CRT_MULTILIB_BDIR}-inst \
      -DCMAKE_C_COMPILER=${INSTALLPREFIX}/bin/clang                      \
      -DCMAKE_CXX_COMPILER=${INSTALLPREFIX}/bin/clang                    \
      -DCMAKE_AR=${INSTALLPREFIX}/bin/llvm-ar                            \
      -DCMAKE_NM=${INSTALLPREFIX}/bin/llvm-nm                            \
      -DCMAKE_RANLIB=${INSTALLPREFIX}/bin/llvm-ranlib                    \
      -DCMAKE_C_COMPILER_TARGET="riscv32-unknown-elf"                    \
      -DCMAKE_CXX_COMPILER_TARGET="riscv32-unknown-elf"                  \
      -DCMAKE_ASM_COMPILER_TARGET="riscv32-unknown-elf"                  \
      -DCMAKE_C_FLAGS="${CRT_MULTILIB_OPT} -O2"                          \
      -DCMAKE_CXX_FLAGS="${CRT_MULTILIB_OPT} -O2"                        \
      -DCMAKE_ASM_FLAGS="${CRT_MULTILIB_OPT} -O2"                        \
      -DCMAKE_EXE_LINKER_FLAGS="-nostartfiles -nostdlib"                 \
      -DCOMPILER_RT_BAREMETAL_BUILD=ON                                   \
      -DCOMPILER_RT_BUILD_BUILTINS=ON                                    \
      -DCOMPILER_RT_BUILD_MEMPROF=OFF                                    \
      -DCOMPILER_RT_BUILD_LIBFUZZER=OFF                                  \
      -DCOMPILER_RT_BUILD_PROFILE=OFF                                    \
      -DCOMPILER_RT_BUILD_SANITIZERS=OFF                                 \
      -DCOMPILER_RT_BUILD_XRAY=OFF                                       \
      -DCOMPILER_RT_DEFAULT_TARGET_ONLY=ON                               \
      -DCOMPILER_RT_OS_DIR=""                                            \
      -DLLVM_CONFIG_PATH=${BUILDPREFIX}/llvm/bin/llvm-config             \
      ${SRCPREFIX}/llvm-project.git/compiler-rt
  make
  make install

  cp ${BUILDPREFIX}/compiler-rt32${CRT_MULTILIB_BDIR}-inst/lib/libclang_rt.builtins-riscv32.a \
     ${INSTALLPREFIX}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/lib/libclang_rt.builtins.a
  cp ${BUILDPREFIX}/compiler-rt32${CRT_MULTILIB_BDIR}-inst/lib/clang_rt.crtbegin-riscv32.o \
     ${INSTALLPREFIX}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/lib/clang_rt.crtbegin.o
  cp ${BUILDPREFIX}/compiler-rt32${CRT_MULTILIB_BDIR}-inst/lib/clang_rt.crtend-riscv32.o \
     ${INSTALLPREFIX}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/lib/libclang_rt.crtend.o
done

for CRT_MULTILIB in $(${BUILDPREFIX}/llvm/bin/clang -target riscv64-unknown-elf -print-multi-lib 2>/dev/null); do
  CRT_MULTILIB_DIR=$(echo ${CRT_MULTILIB} | sed 's/;.*//')
  CRT_MULTILIB_OPT=$(echo ${CRT_MULTILIB} | sed 's/.*;//' | sed 's/@/-/' | sed 's/@/ -/g')
  CRT_MULTILIB_BDIR=$(echo ${CRT_MULTILIB} | sed 's/.*;//' | sed 's/@/_/g')
  echo "Multilib: \"${CRT_MULTILIB_DIR}\" -> \"${CRT_MULTILIB_OPT}\""

  mkdir -p ${BUILDPREFIX}/compiler-rt64${CRT_MULTILIB_BDIR}
  cd ${BUILDPREFIX}/compiler-rt64${CRT_MULTILIB_BDIR}
  cmake -G"Unix Makefiles"                                                     \
      -DCMAKE_SYSTEM_NAME=Linux                                                \
      -DCMAKE_INSTALL_PREFIX=${BUILDPREFIX}/compiler-rt64${CRT_MULTILIB_BDIR}-inst \
      -DCMAKE_C_COMPILER=${INSTALLPREFIX}/bin/clang                      \
      -DCMAKE_CXX_COMPILER=${INSTALLPREFIX}/bin/clang                    \
      -DCMAKE_AR=${INSTALLPREFIX}/bin/llvm-ar                            \
      -DCMAKE_NM=${INSTALLPREFIX}/bin/llvm-nm                            \
      -DCMAKE_RANLIB=${INSTALLPREFIX}/bin/llvm-ranlib                    \
      -DCMAKE_C_COMPILER_TARGET="riscv64-unknown-elf"                    \
      -DCMAKE_CXX_COMPILER_TARGET="riscv64-unknown-elf"                  \
      -DCMAKE_ASM_COMPILER_TARGET="riscv64-unknown-elf"                  \
      -DCMAKE_C_FLAGS="${CRT_MULTILIB_OPT} -O2"                          \
      -DCMAKE_CXX_FLAGS="${CRT_MULTILIB_OPT} -O2"                        \
      -DCMAKE_ASM_FLAGS="${CRT_MULTILIB_OPT} -O2"                        \
      -DCMAKE_EXE_LINKER_FLAGS="-nostartfiles -nostdlib"                 \
      -DCOMPILER_RT_BAREMETAL_BUILD=ON                                   \
      -DCOMPILER_RT_BUILD_BUILTINS=ON                                    \
      -DCOMPILER_RT_BUILD_MEMPROF=OFF                                    \
      -DCOMPILER_RT_BUILD_LIBFUZZER=OFF                                  \
      -DCOMPILER_RT_BUILD_PROFILE=OFF                                    \
      -DCOMPILER_RT_BUILD_SANITIZERS=OFF                                 \
      -DCOMPILER_RT_BUILD_XRAY=OFF                                       \
      -DCOMPILER_RT_DEFAULT_TARGET_ONLY=ON                               \
      -DCOMPILER_RT_OS_DIR=""                                            \
      -DLLVM_CONFIG_PATH=${BUILDPREFIX}/llvm/bin/llvm-config             \
      ${SRCPREFIX}/llvm-project.git/compiler-rt
  make
  make install

  cp ${BUILDPREFIX}/compiler-rt64${CRT_MULTILIB_BDIR}-inst/lib/libclang_rt.builtins-riscv64.a \
     ${INSTALLPREFIX}/lib/clang-runtimes/riscv64-unknown-elf/${CRT_MULTILIB_DIR}/lib/libclang_rt.builtins.a
  cp ${BUILDPREFIX}/compiler-rt64${CRT_MULTILIB_BDIR}-inst/lib/clang_rt.crtbegin-riscv64.o \
     ${INSTALLPREFIX}/lib/clang-runtimes/riscv64-unknown-elf/${CRT_MULTILIB_DIR}/lib/clang_rt.crtbegin.o
  cp ${BUILDPREFIX}/compiler-rt64${CRT_MULTILIB_BDIR}-inst/lib/clang_rt.crtend-riscv64.o \
     ${INSTALLPREFIX}/lib/clang-runtimes/riscv64-unknown-elf/${CRT_MULTILIB_DIR}/lib/libclang_rt.crtend.o
done

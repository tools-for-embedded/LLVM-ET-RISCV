# Build the clang-runtimes directory tree for each multilib
for CRT_MULTILIB in $(${BUILDPREFIX}/llvm/bin/clang -target riscv32-unknown-elf -print-multi-lib 2>/dev/null); do
  CRT_MULTILIB_DIR=$(echo ${CRT_MULTILIB} | sed 's/;.*//')
  mkdir -p ${INSTALLPREFIX}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/lib
  cp ${BUILDPREFIX}/newlib32-inst/riscv32-unknown-elf/lib/${CRT_MULTILIB_DIR}/*.a \
      ${INSTALLPREFIX}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/lib
  cp ${BUILDPREFIX}/newlib32-inst/riscv32-unknown-elf/lib/${CRT_MULTILIB_DIR}/*.o \
      ${INSTALLPREFIX}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/lib
  for file in libc.a libm.a libg.a libgloss.a; do
    cp ${BUILDPREFIX}/newlib32-nano-inst/riscv32-unknown-elf/lib/${CRT_MULTILIB_DIR}/${file} \
        ${INSTALLPREFIX}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/lib/${file%.*}_nano.${file##*.}
  done
  cp ${BUILDPREFIX}/newlib32-nano-inst/riscv32-unknown-elf/lib/${CRT_MULTILIB_DIR}/crt0.o \
      ${INSTALLPREFIX}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/libcrt0.o
  rsync -a ${BUILDPREFIX}/newlib32-inst/riscv32-unknown-elf/include/ \
      ${INSTALLPREFIX}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/include/
  mkdir ${INSTALLPREFIX}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/include/newlib-nano
  cp ${BUILDPREFIX}/newlib32-nano-inst/riscv32-unknown-elf/include/newlib.h \
      ${INSTALLPREFIX}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/include/newlib-nano/newlib.h
done

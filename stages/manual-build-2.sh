#!/usr/bin/env bash
# Add symlinks to LLVM tools
cd ${INSTALLPREFIX}/bin
for TRIPLE in riscv32-unknown-elf riscv64-unknown-elf; do
  for TOOL in clang clang++ cc c++; do
    ln -sv clang ${TRIPLE}-${TOOL}
  done
done
ln -sfv llvm-readobj llvm-readelf
ln -sfv llvm-objcopy llvm-strip

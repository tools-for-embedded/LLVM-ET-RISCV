#!/usr/bin/env bash
# Clang/LLVM
mkdir -p ${BUILDPREFIX}/llvm
cd ${BUILDPREFIX}/llvm
cmake -G"Unix Makefiles"                                         \
-DCMAKE_BUILD_TYPE=Release                                   \
-DCMAKE_INSTALL_PREFIX=${INSTALLPREFIX}                      \
-DLLVM_ENABLE_PROJECTS=clang\;lld                            \
-DLLVM_ENABLE_PLUGINS=ON                                     \
-DLLVM_DISTRIBUTION_COMPONENTS=clang\;clang-resource-headers\;lld\;llvm-ar\;llvm-cov\;llvm-cxxfilt\;llvm-dwp\;llvm-ranlib\;llvm-nm\;llvm-objcopy\;llvm-objdump\;llvm-readobj\;llvm-size\;llvm-strings\;llvm-strip\;llvm-profdata\;llvm-symbolizer \
-DLLVM_PARALLEL_LINK_JOBS=5                                  \
-DLLVM_TARGETS_TO_BUILD=${LLVM_NATIVE_ARCH}\;RISCV           \
${EXTRA_LLVM_OPTS}                                           \
-S ${SRCPREFIX}/llvm-project.git/llvm

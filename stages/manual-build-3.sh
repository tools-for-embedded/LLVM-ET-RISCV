# Newlib - build for rv32
PATH=${INSTALLPREFIX}/bin:${PATH}
mkdir -p ${BUILDPREFIX}/newlib32
cd ${BUILDPREFIX}/newlib32
CC_FOR_TARGET="clang -target riscv32-unknown-elf"  \
AR_FOR_TARGET=llvm-ar                              \
NM_FOR_TARGET=llvm-nm                              \
RANLIB_FOR_TARGET=llvm-ranlib                      \
READELF_FOR_TARGET=llvm-readelf                    \
STRIP_FOR_TARGET=llvm-strip                        \
CFLAGS_FOR_TARGET="-O2 -mcmodel=medany -Wno-error=implicit-function-declaration -Wno-int-conversion" \
${SRCPREFIX}/newlib-cygwin.git/configure                             \
--target=riscv32-unknown-elf                   \
--prefix=${BUILDPREFIX}/newlib32-inst          \
--enable-multilib                              \
--enable-newlib-io-long-double                 \
--enable-newlib-io-long-long                   \
--enable-newlib-io-c99-formats                 \
--enable-newlib-register-fini                  \
${EXTRA_NEWLIB_OPTS}
make
make install

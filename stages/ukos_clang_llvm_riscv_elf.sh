#!/usr/bin/env bash

# ukos_clang_llvm_riscv_elf.
# ==========================

#------------------------------------------------------------------------
# Author:	Laurent von Allmen	The  2024-03-21
# Modifs:
#
# Project:	uKOS-IV
# Goal:     Toolchain for generating clang cross compilers
#           for Unix like machines
#			git clone --depth 1 https://github.com/llvm/llvm-project.git
#
#           Usage:
#			./ukos_clang_llvm_riscv_elf.sh
#
#           OS:
#           OSX 14.xx           yes
#           Ubuntu 23.04 LTS    yes
#
#   (c) 2023-2024, Laurent von Allmen
#   ---------------------------------
#                                              __ ______  _____
#   Edo. Franzi                         __  __/ //_/ __ \/ ___/
#   5-Route de Cheseaux                / / / / ,< / / / /\__ \
#   CH 1400 Cheseaux-Noréaz           / /_/ / /| / /_/ /___/ /
#                                     \__,_/_/ |_\____//____/
#   edo.franzi@ukos.ch
#
#   Licensed to the Apache Software Foundation (ASF) under one or more
#   contributor license agreements. See the NOTICE file distributed with
#   this work for additional information regarding copyright ownership.
#   The ASF licenses this file to you under the Apache License, Version 2.0
#   (the "License"); you may not use this file except in compliance with
#   the License. You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#------------------------------------------------------------------------

# Adapted from build-riscv32-clang-baremetal.sh of Embecosm
#
# Copyright (C) 2020 Embecosm Limited
#
# Contributor: Simon Cook <simon.cook@embecosm.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
set -e

if [ -z "$PATH_TOOLS_ROOT" ]; then
    echo "Variable PATH_TOOLS_ROOT is not set!"
    exit 1
fi

if [ -z "$CLANG_RVXX_VER" ]; then
	echo "Variable CLANG_RVXX_VER is not set!"
	exit 1
fi

if [ -z "$NLB_VER" ]; then
	echo "Variable NLB_VER is not set!"
	exit 1
fi

mkdir -p ${PATH_TOOLS_ROOT}
mkdir -p ${PATH_TOOLS_ROOT}/Packages

# Environment
# -----------

export PACKS_CLANG=${PATH_TOOLS_ROOT}/Packages/clang-${CLANG_RVXX_VER}-riscv
export PACKS_NLB=${PATH_TOOLS_ROOT}/Packages/newlib-${NLB_VER}

export BUILD=${PATH_TOOLS_ROOT}/builds/llvm-${CLANG_RVXX_VER}/riscv
export CROSS=${PATH_TOOLS_ROOT}/cross/llvm-${CLANG_RVXX_VER}/riscv

# Downloading sources
# -------------------

if [ ! -d "${PACKS_CLANG}" ]; then
	echo Downloading clang-${CLANG_RVXX_VER}
	git clone --depth 1 --branch llvmorg-${CLANG_RVXX_VER} https://github.com/llvm/llvm-project.git ${PACKS_CLANG}
fi
if [ ! -d "${PACKS_NLB}" ]; then
	echo Downloading newlib-${NLB_VER}
	git clone --depth 1 --branch newlib-${NLB_VER} https://sourceware.org/git/newlib-cygwin.git ${PACKS_NLB}
fi

CURRENTTIME=`date`
PKGVERS="riscv-llvm-embedded-macos-${CURRENTTIME}"
export EXTRA_LLVM_OPTS="${EXTRA_LLVM_OPTS} -DCLANG_VENDOR='${PKGVERS}'"

# Attempt to identify the host architecture, and include this in the build

if [ "$(uname -m)" == "arm64" ]; then
  LLVM_NATIVE_ARCH="AArch64"
else
  LLVM_NATIVE_ARCH="X86"
fi

EXTRA_NEWLIB_OPTS=" \
	--enable-interwork \
	--enable-newlib-io-float \
	--disable-werror \
	--disable-nls \
	--disable-libssp"

# Building the toolchain
# ----------------------

rm -rf ${BUILD}
rm -rf ${CROSS}
mkdir -p ${BUILD}
mkdir -p ${CROSS}

echo "Start of build:" > ${BUILD}/clang_llvm_riscv_temp.txt
date >> ${BUILD}/clang_llvm_riscv_temp.txt

# Clang/LLVM

mkdir -p ${BUILD}/llvm
cd ${BUILD}/llvm
cmake -G"Unix Makefiles" \
-DCMAKE_BUILD_TYPE=Release \
-DCMAKE_INSTALL_PREFIX=${CROSS} \
-DLLVM_ENABLE_PROJECTS="clang;lld" \
-DLLVM_ENABLE_PLUGINS=ON \
-DLLVM_DISTRIBUTION_COMPONENTS="clang;clang-resource-headers;lld;llvm-ar;llvm-cov;llvm-cxxfilt;llvm-dwp;llvm-ranlib;llvm-nm;llvm-objcopy;llvm-objdump;llvm-readobj;llvm-size;llvm-strings;llvm-strip;llvm-profdata;llvm-symbolizer" \
-DLLVM_PARALLEL_LINK_JOBS=5 \
-DLLVM_TARGETS_TO_BUILD=${LLVM_NATIVE_ARCH}\;RISCV \
${EXTRA_LLVM_OPTS} \
-S ${PACKS_CLANG}/llvm
make
make install-distribution

# Add symlinks to LLVM tools

cd ${CROSS}/bin
for TRIPLE in riscv32-unknown-elf riscv64-unknown-elf; do
  for TOOL in clang clang++ cc c++; do
    ln -sv clang ${TRIPLE}-${TOOL}
  done
done
ln -sfv llvm-readobj llvm-readelf
ln -sfv llvm-objcopy llvm-strip

# Newlib - build for rv32

PATH=${CROSS}/bin:${PATH}
mkdir -p ${BUILD}/newlib32
cd ${BUILD}/newlib32
CC_FOR_TARGET="clang -target riscv32-unknown-elf"  \
AR_FOR_TARGET=llvm-ar                              \
NM_FOR_TARGET=llvm-nm                              \
RANLIB_FOR_TARGET=llvm-ranlib                      \
READELF_FOR_TARGET=llvm-readelf                    \
STRIP_FOR_TARGET=llvm-strip                        \
CFLAGS_FOR_TARGET="-O2 -mcmodel=medany -Wno-error=implicit-function-declaration -Wno-int-conversion" \
${PACKS_NLB}/configure                             \
--target=riscv32-unknown-elf                   \
--prefix=${BUILD}/newlib32-inst          \
--enable-multilib                              \
--enable-newlib-io-long-double                 \
--enable-newlib-io-long-long                   \
--enable-newlib-io-c99-formats                 \
--enable-newlib-register-fini                  \
${EXTRA_NEWLIB_OPTS}
make -j4
make install

mkdir -p ${BUILD}/newlib32-nano
cd ${BUILD}/newlib32-nano
CC_FOR_TARGET="clang -target riscv32-unknown-elf"  \
AR_FOR_TARGET=llvm-ar                              \
NM_FOR_TARGET=llvm-nm                              \
RANLIB_FOR_TARGET=llvm-ranlib                      \
READELF_FOR_TARGET=llvm-readelf                    \
STRIP_FOR_TARGET=llvm-strip                        \
CFLAGS_FOR_TARGET="-Os -mcmodel=medany -ffunction-sections -fdata-sections -Wno-error=implicit-function-declaration -Wno-int-conversion" \
${PACKS_NLB}/configure                             \
--target=riscv32-unknown-elf                   \
--prefix=${BUILD}/newlib32-nano-inst     \
--enable-multilib                              \
--enable-newlib-reent-small                    \
--disable-newlib-fvwrite-in-streamio           \
--disable-newlib-fseek-optimization            \
--disable-newlib-wide-orient                   \
--enable-newlib-nano-malloc                    \
--disable-newlib-unbuf-stream-opt              \
--enable-lite-exit                             \
--enable-newlib-global-atexit                  \
--enable-newlib-nano-formatted-io              \
--disable-newlib-supplied-syscalls             \
--disable-nls                                  \
${EXTRA_NEWLIB_OPTS}
make
make install

# Build the clang-runtimes directory tree for each multilib

for CRT_MULTILIB in $(${BUILD}/llvm/bin/clang -target riscv32-unknown-elf -print-multi-lib 2>/dev/null); do
  CRT_MULTILIB_DIR=$(echo ${CRT_MULTILIB} | sed 's/;.*//')
  mkdir -p ${CROSS}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/lib
  cp ${BUILD}/newlib32-inst/riscv32-unknown-elf/lib/${CRT_MULTILIB_DIR}/*.a \
      ${CROSS}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/lib
  cp ${BUILD}/newlib32-inst/riscv32-unknown-elf/lib/${CRT_MULTILIB_DIR}/*.o \
      ${CROSS}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/lib
  for file in libc.a libm.a libg.a libgloss.a; do
    cp ${BUILD}/newlib32-nano-inst/riscv32-unknown-elf/lib/${CRT_MULTILIB_DIR}/${file} \
        ${CROSS}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/lib/${file%.*}_nano.${file##*.}
  done
  cp ${BUILD}/newlib32-nano-inst/riscv32-unknown-elf/lib/${CRT_MULTILIB_DIR}/crt0.o \
      ${CROSS}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/libcrt0.o
  rsync -a ${BUILD}/newlib32-inst/riscv32-unknown-elf/include/ \
      ${CROSS}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/include/
  mkdir ${CROSS}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/include/newlib-nano
  cp ${BUILD}/newlib32-nano-inst/riscv32-unknown-elf/include/newlib.h \
      ${CROSS}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/include/newlib-nano/newlib.h
done

# Newlib - build for rv64

PATH=${CROSS}/bin:${PATH}
mkdir -p ${BUILD}/newlib64
cd ${BUILD}/newlib64
CC_FOR_TARGET="clang -target riscv64-unknown-elf"  \
AR_FOR_TARGET=llvm-ar                              \
NM_FOR_TARGET=llvm-nm                              \
RANLIB_FOR_TARGET=llvm-ranlib                      \
READELF_FOR_TARGET=llvm-readelf                    \
STRIP_FOR_TARGET=llvm-strip                        \
CFLAGS_FOR_TARGET="-O2 -mcmodel=medany -Wno-error=implicit-function-declaration -Wno-int-conversion" \
${PACKS_NLB}/configure       \
--target=riscv64-unknown-elf                   \
--prefix=${BUILD}/newlib64-inst          \
--enable-multilib                              \
--enable-newlib-io-long-double                 \
--enable-newlib-io-long-long                   \
--enable-newlib-io-c99-formats                 \
--enable-newlib-register-fini                  \
${EXTRA_NEWLIB_OPTS}
make
make install

mkdir -p ${BUILD}/newlib64-nano
cd ${BUILD}/newlib64-nano
CC_FOR_TARGET="clang -target riscv64-unknown-elf"  \
AR_FOR_TARGET=llvm-ar                              \
NM_FOR_TARGET=llvm-nm                              \
RANLIB_FOR_TARGET=llvm-ranlib                      \
READELF_FOR_TARGET=llvm-readelf                    \
STRIP_FOR_TARGET=llvm-strip                        \
CFLAGS_FOR_TARGET="-Os -mcmodel=medany -ffunction-sections -fdata-sections -Wno-error=implicit-function-declaration -Wno-int-conversion" \
${PACKS_NLB}/configure       \
--target=riscv64-unknown-elf                   \
--prefix=${BUILD}/newlib64-nano-inst     \
--enable-multilib                              \
--enable-newlib-reent-small                    \
--disable-newlib-fvwrite-in-streamio           \
--disable-newlib-fseek-optimization            \
--disable-newlib-wide-orient                   \
--enable-newlib-nano-malloc                    \
--disable-newlib-unbuf-stream-opt              \
--enable-lite-exit                             \
--enable-newlib-global-atexit                  \
--enable-newlib-nano-formatted-io              \
--disable-newlib-supplied-syscalls             \
--disable-nls                                  \
${EXTRA_NEWLIB_OPTS}
make
make install

# Build the clang-runtimes directory tree for each multilib

for CRT_MULTILIB in $(${BUILD}/llvm/bin/clang -target riscv64-unknown-elf -print-multi-lib 2>/dev/null); do
  CRT_MULTILIB_DIR=$(echo ${CRT_MULTILIB} | sed 's/;.*//')
  mkdir -p ${CROSS}/lib/clang-runtimes/riscv64-unknown-elf/${CRT_MULTILIB_DIR}/lib
  cp ${BUILD}/newlib64-inst/riscv64-unknown-elf/lib/${CRT_MULTILIB_DIR}/*.a \
      ${CROSS}/lib/clang-runtimes/riscv64-unknown-elf/${CRT_MULTILIB_DIR}/lib
  cp ${BUILD}/newlib64-inst/riscv64-unknown-elf/lib/${CRT_MULTILIB_DIR}/*.o \
      ${CROSS}/lib/clang-runtimes/riscv64-unknown-elf/${CRT_MULTILIB_DIR}/lib
  for file in libc.a libm.a libg.a libgloss.a; do
    cp ${BUILD}/newlib64-nano-inst/riscv64-unknown-elf/lib/${CRT_MULTILIB_DIR}/${file} \
        ${CROSS}/lib/clang-runtimes/riscv64-unknown-elf/${CRT_MULTILIB_DIR}/lib/${file%.*}_nano.${file##*.}
  done
  cp ${BUILD}/newlib64-nano-inst/riscv64-unknown-elf/lib/${CRT_MULTILIB_DIR}/crt0.o \
      ${CROSS}/lib/clang-runtimes/riscv64-unknown-elf/${CRT_MULTILIB_DIR}/libcrt0.o
  rsync -a ${BUILD}/newlib64-inst/riscv64-unknown-elf/include/ \
      ${CROSS}/lib/clang-runtimes/riscv64-unknown-elf/${CRT_MULTILIB_DIR}/include/
  mkdir ${CROSS}/lib/clang-runtimes/riscv64-unknown-elf/${CRT_MULTILIB_DIR}/include/newlib-nano
  cp ${BUILD}/newlib64-nano-inst/riscv64-unknown-elf/include/newlib.h \
      ${CROSS}/lib/clang-runtimes/riscv64-unknown-elf/${CRT_MULTILIB_DIR}/include/newlib-nano/newlib.h
done

# Compiler-rt for rv32 and rv64
# NOTE: CMAKE_SYSTEM_NAME is set to linux to allow the configure step to
#       correctly validate that clang works for cross compiling

for CRT_MULTILIB in $(${BUILD}/llvm/bin/clang -target riscv32-unknown-elf -print-multi-lib 2>/dev/null); do
  CRT_MULTILIB_DIR=$(echo ${CRT_MULTILIB} | sed 's/;.*//')
  CRT_MULTILIB_OPT=$(echo ${CRT_MULTILIB} | sed 's/.*;//' | sed 's/@/-/' | sed 's/@/ -/g')
  CRT_MULTILIB_BDIR=$(echo ${CRT_MULTILIB} | sed 's/.*;//' | sed 's/@/_/g')
  echo "Multilib: \"${CRT_MULTILIB_DIR}\" -> \"${CRT_MULTILIB_OPT}\""

  mkdir -p ${BUILD}/compiler-rt32${CRT_MULTILIB_BDIR}
  cd ${BUILD}/compiler-rt32${CRT_MULTILIB_BDIR}
  cmake -G"Unix Makefiles"                                                     \
      -DCMAKE_SYSTEM_NAME=Linux                                                \
      -DCMAKE_INSTALL_PREFIX=${BUILD}/compiler-rt32${CRT_MULTILIB_BDIR}-inst \
      -DCMAKE_C_COMPILER=${CROSS}/bin/clang                      \
      -DCMAKE_CXX_COMPILER=${CROSS}/bin/clang                    \
      -DCMAKE_AR=${CROSS}/bin/llvm-ar                            \
      -DCMAKE_NM=${CROSS}/bin/llvm-nm                            \
      -DCMAKE_RANLIB=${CROSS}/bin/llvm-ranlib                    \
      -DCMAKE_C_COMPILER_TARGET="riscv32-unknown-elf"                    \
      -DCMAKE_CXX_COMPILER_TARGET="riscv32-unknown-elf"                  \
      -DCMAKE_ASM_COMPILER_TARGET="riscv32-unknown-elf"                  \
      -DCMAKE_C_FLAGS="${CRT_MULTILIB_OPT} -O2"                          \
      -DCMAKE_CXX_FLAGS="${CRT_MULTILIB_OPT} -O2"                        \
      -DCMAKE_ASM_FLAGS="${CRT_MULTILIB_OPT} -O2"                        \
      -DCMAKE_EXE_LINKER_FLAGS="-nostartfiles -nostdlib"                 \
      -DCOMPILER_RT_BAREMETAL_BUILD=ON                                   \
      -DCOMPILER_RT_BUILD_BUILTINS=ON                                    \
      -DCOMPILER_RT_BUILD_MEMPROF=OFF                                    \
      -DCOMPILER_RT_BUILD_LIBFUZZER=OFF                                  \
      -DCOMPILER_RT_BUILD_PROFILE=OFF                                    \
      -DCOMPILER_RT_BUILD_SANITIZERS=OFF                                 \
      -DCOMPILER_RT_BUILD_XRAY=OFF                                       \
      -DCOMPILER_RT_DEFAULT_TARGET_ONLY=ON                               \
      -DCOMPILER_RT_OS_DIR=""                                            \
      -DLLVM_CONFIG_PATH=${BUILD}/llvm/bin/llvm-config             \
      ${PACKS_CLANG}/compiler-rt
  make
  make install

  cp ${BUILD}/compiler-rt32${CRT_MULTILIB_BDIR}-inst/lib/libclang_rt.builtins-riscv32.a \
     ${CROSS}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/lib/libclang_rt.builtins.a
  cp ${BUILD}/compiler-rt32${CRT_MULTILIB_BDIR}-inst/lib/clang_rt.crtbegin-riscv32.o \
     ${CROSS}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/lib/clang_rt.crtbegin.o
  cp ${BUILD}/compiler-rt32${CRT_MULTILIB_BDIR}-inst/lib/clang_rt.crtend-riscv32.o \
     ${CROSS}/lib/clang-runtimes/riscv32-unknown-elf/${CRT_MULTILIB_DIR}/lib/libclang_rt.crtend.o
done

for CRT_MULTILIB in $(${BUILD}/llvm/bin/clang -target riscv64-unknown-elf -print-multi-lib 2>/dev/null); do
  CRT_MULTILIB_DIR=$(echo ${CRT_MULTILIB} | sed 's/;.*//')
  CRT_MULTILIB_OPT=$(echo ${CRT_MULTILIB} | sed 's/.*;//' | sed 's/@/-/' | sed 's/@/ -/g')
  CRT_MULTILIB_BDIR=$(echo ${CRT_MULTILIB} | sed 's/.*;//' | sed 's/@/_/g')
  echo "Multilib: \"${CRT_MULTILIB_DIR}\" -> \"${CRT_MULTILIB_OPT}\""

  mkdir -p ${BUILD}/compiler-rt64${CRT_MULTILIB_BDIR}
  cd ${BUILD}/compiler-rt64${CRT_MULTILIB_BDIR}
  cmake -G"Unix Makefiles"                                                     \
      -DCMAKE_SYSTEM_NAME=Linux                                                \
      -DCMAKE_INSTALL_PREFIX=${BUILD}/compiler-rt64${CRT_MULTILIB_BDIR}-inst \
      -DCMAKE_C_COMPILER=${CROSS}/bin/clang                      \
      -DCMAKE_CXX_COMPILER=${CROSS}/bin/clang                    \
      -DCMAKE_AR=${CROSS}/bin/llvm-ar                            \
      -DCMAKE_NM=${CROSS}/bin/llvm-nm                            \
      -DCMAKE_RANLIB=${CROSS}/bin/llvm-ranlib                    \
      -DCMAKE_C_COMPILER_TARGET="riscv64-unknown-elf"                    \
      -DCMAKE_CXX_COMPILER_TARGET="riscv64-unknown-elf"                  \
      -DCMAKE_ASM_COMPILER_TARGET="riscv64-unknown-elf"                  \
      -DCMAKE_C_FLAGS="${CRT_MULTILIB_OPT} -O2"                          \
      -DCMAKE_CXX_FLAGS="${CRT_MULTILIB_OPT} -O2"                        \
      -DCMAKE_ASM_FLAGS="${CRT_MULTILIB_OPT} -O2"                        \
      -DCMAKE_EXE_LINKER_FLAGS="-nostartfiles -nostdlib"                 \
      -DCOMPILER_RT_BAREMETAL_BUILD=ON                                   \
      -DCOMPILER_RT_BUILD_BUILTINS=ON                                    \
      -DCOMPILER_RT_BUILD_MEMPROF=OFF                                    \
      -DCOMPILER_RT_BUILD_LIBFUZZER=OFF                                  \
      -DCOMPILER_RT_BUILD_PROFILE=OFF                                    \
      -DCOMPILER_RT_BUILD_SANITIZERS=OFF                                 \
      -DCOMPILER_RT_BUILD_XRAY=OFF                                       \
      -DCOMPILER_RT_DEFAULT_TARGET_ONLY=ON                               \
      -DCOMPILER_RT_OS_DIR=""                                            \
      -DLLVM_CONFIG_PATH=${BUILD}/llvm/bin/llvm-config             \
      ${PACKS_CLANG}/compiler-rt
  make
  make install

  cp ${BUILD}/compiler-rt64${CRT_MULTILIB_BDIR}-inst/lib/libclang_rt.builtins-riscv64.a \
     ${CROSS}/lib/clang-runtimes/riscv64-unknown-elf/${CRT_MULTILIB_DIR}/lib/libclang_rt.builtins.a
  cp ${BUILD}/compiler-rt64${CRT_MULTILIB_BDIR}-inst/lib/clang_rt.crtbegin-riscv64.o \
     ${CROSS}/lib/clang-runtimes/riscv64-unknown-elf/${CRT_MULTILIB_DIR}/lib/clang_rt.crtbegin.o
  cp ${BUILD}/compiler-rt64${CRT_MULTILIB_BDIR}-inst/lib/clang_rt.crtend-riscv64.o \
     ${CROSS}/lib/clang-runtimes/riscv64-unknown-elf/${CRT_MULTILIB_DIR}/lib/libclang_rt.crtend.o
done

echo "End of build:" >> ${BUILD}/clang_llvm_riscv_temp.txt
date >> ${BUILD}/clang_llvm_riscv_temp.txt
mv ${BUILD}/clang_llvm_riscv_temp.txt ${BUILD}/clang_llvm_riscv_ready.txt

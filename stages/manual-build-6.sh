# Newlib - build for rv64
PATH=${INSTALLPREFIX}/bin:${PATH}
mkdir -p ${BUILDPREFIX}/newlib64
cd ${BUILDPREFIX}/newlib64
CC_FOR_TARGET="clang -target riscv64-unknown-elf"  \
AR_FOR_TARGET=llvm-ar                              \
NM_FOR_TARGET=llvm-nm                              \
RANLIB_FOR_TARGET=llvm-ranlib                      \
READELF_FOR_TARGET=llvm-readelf                    \
STRIP_FOR_TARGET=llvm-strip                        \
CFLAGS_FOR_TARGET="-O2 -mcmodel=medany -Wno-error=implicit-function-declaration -Wno-int-conversion" \
${SRCPREFIX}/newlib-cygwin.git/configure       \
--target=riscv64-unknown-elf                   \
--prefix=${BUILDPREFIX}/newlib64-inst          \
--enable-multilib                              \
--enable-newlib-io-long-double                 \
--enable-newlib-io-long-long                   \
--enable-newlib-io-c99-formats                 \
--enable-newlib-register-fini                  \
${EXTRA_NEWLIB_OPTS}
make
make install

mkdir -p ${BUILDPREFIX}/newlib64-nano
cd ${BUILDPREFIX}/newlib64-nano
CC_FOR_TARGET="clang -target riscv64-unknown-elf"  \
AR_FOR_TARGET=llvm-ar                              \
NM_FOR_TARGET=llvm-nm                              \
RANLIB_FOR_TARGET=llvm-ranlib                      \
READELF_FOR_TARGET=llvm-readelf                    \
STRIP_FOR_TARGET=llvm-strip                        \
CFLAGS_FOR_TARGET="-Os -mcmodel=medany -ffunction-sections -fdata-sections -Wno-error=implicit-function-declaration -Wno-int-conversion" \
${SRCPREFIX}/newlib-cygwin.git/configure       \
--target=riscv64-unknown-elf                   \
--prefix=${BUILDPREFIX}/newlib64-nano-inst     \
--enable-multilib                              \
--enable-newlib-reent-small                    \
--disable-newlib-fvwrite-in-streamio           \
--disable-newlib-fseek-optimization            \
--disable-newlib-wide-orient                   \
--enable-newlib-nano-malloc                    \
--disable-newlib-unbuf-stream-opt              \
--enable-lite-exit                             \
--enable-newlib-global-atexit                  \
--enable-newlib-nano-formatted-io              \
--disable-newlib-supplied-syscalls             \
--disable-nls                                  \
${EXTRA_NEWLIB_OPTS}
make
make install
